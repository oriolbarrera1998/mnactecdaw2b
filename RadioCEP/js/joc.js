const pregunta = document.getElementById("pregunta");
const opcions = Array.from(document.getElementsByClassName("textOpcio"));
const contPreguntesText = document.getElementById("barraText");
const puntsText = document.getElementById("punts");
const estatBarraProgres = document.getElementById("estatBarraProgres");


let preguntaActual = {};
let respondre = true;
let punts = 0;
let contPreguntes = 0;
let llistaPreguntes = [];


fetch("../utils/preguntes.json").then(res =>{
    return res.json();
}).then(preguntesJson =>{
    preguntes = preguntesJson;
    
    comencarJoc();
});

const PUNTS_ENCERTAR = 10;
const NUM_PREGUNTES = 3;
const TEMPS_PREGUNTA = 30;

//Funció per inicialitzar el joc
comencarJoc = () => {
    comencarConta();
    contPreguntes = 0;
    punts = 0;
    llistaPreguntes = [ ... preguntes];
    console.log(llistaPreguntes);
    novaPregunta();
};

novaPregunta = () => {

    if(llistaPreguntes.length === 0 || contPreguntes >= NUM_PREGUNTES){
        localStorage.setItem("puntuacioFinal2", punts);
        return window.location.assign("final.html");
    };

    contPreguntes++;

    barraText.innerText = `Pregunta ${contPreguntes}/${NUM_PREGUNTES}`;

    estatBarraProgres.style.width = `${(contPreguntes / NUM_PREGUNTES) * 100}%`;

    const indexPregunta = Math.floor(Math.random() * llistaPreguntes.length);
    preguntaActual = llistaPreguntes[indexPregunta];
    pregunta.innerText = preguntaActual.pregunta;
    opcions.forEach( opcio => {
        const numResposta = opcio.dataset["numero"];
        opcio.innerText = preguntaActual["opcio" + numResposta];
    });

    llistaPreguntes.splice(indexPregunta, 1);

    respondre = true;
};

opcions.forEach(opcio => {
    opcio.addEventListener("click", e => {
        if(!respondre) return;

        respondre = false;
        const opcioEscollida = e.target;
        const respostaEscollida = opcioEscollida.dataset["numero"];

        const feedbackResposta = respostaEscollida == preguntaActual.resposta ? "correcte" : "incorrecte";
    

        if(feedbackResposta === "correcte") {
            sumarPunts(PUNTS_ENCERTAR);
        };
        
        opcioEscollida.parentElement.classList.add(feedbackResposta);

        //Aquesta funció serveix per indicar un temps fins que surt la següent pregunta perquè es pugui veure si la resposta donada es correcte o incorrecte.
        setTimeout(() => {
            opcioEscollida.parentElement.classList.remove(feedbackResposta);
            novaPregunta();
        }, 1000);
    });
});

sumarPunts = (num) => {
    punts += num;
    puntsText.innerText = punts;
}

contaEnrera = (duration, display) => {
    var contador = duration, seconds;
    setInterval(function () {
        seconds = parseInt(contador % 60, 10);
        
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = seconds;

        if (--contador < 0) {
          contador = duration;
        }
    }, 1000);
}

comencarConta = () => {
    var thirtySeconds = 59, display = document.querySelector('#temps');
    contaEnrera(thirtySeconds, display);
};


